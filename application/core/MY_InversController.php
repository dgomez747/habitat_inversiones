<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_InversController extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// $this->load->model(['Template_model' => 'Template']);
	}

	// public function send_update($id, $items){
	// 	$resp = $this->Template->update($id, $items);
	// 	$message = ($resp)?  "Actualizacion exitoso.":"Error al insertar los datos verifique los datos correctamente";
	// 	$this->sender($resp, $message);
	// }

	// public function send_delete($items){
	// 	$resp = $this->Template->destroy($items);
	// 	$message = ($resp)?  "Eliminacion exitoso.":"Error al intentar eliminar";
	// 	$this->sender($resp, $message);
	// }

	// public function send_create($items){
	// 	$resp = $this->Template->insert($items);
	// 	$message = ($resp)?  "Registro exitoso.":"Error al insertar los datos verifique los datos correctamente";
	// 	$this->sender($resp, $message);
	// }

	// private function sender($resp , $message){
	// 	if ($resp):
	// 		$this->response_send(['status' => 200, 'response' => ['result' => true, 'message' => $message]]);
	// 	else:
	// 		$this->response_send(['status' => 422, 'response' => ['result' => true, 'message' => $message, 'errors' => null]]);
	// 	endif;

	// }

	public function response_send($array, $content_type = 'application/json') {
		return $this->output->set_content_type($content_type)->set_status_header($array['status'])->set_output(json_encode($array['response']));
	}

}
