<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

	class  MY_Controller extends MX_Controller {
		public function __construct() {
			parent::__construct();
		}

		public function get_sesion() {
			if (isset($_SESSION['usercms'])) {
				if ($_SESSION['usercms']['logged_usercms'] == "TRUE") {
					$_SESSION['usercms']['js'] = array();
					$_SESSION['usercms']['css'] = array();
					return true;
				} else {
					redirect(site_url() . 'login');
				}
			} else {
				redirect(site_url() . 'login');
			}
		}

		public function nullable($items) {
			foreach ($items as $key => $item):
				$items[$key] = empty($item) ? null : $item;
			endforeach;
			return $items;
		}

		public function pluck($items) {
			$elements = [];
			foreach ($items as $key => $element):
				$elements[$element->id] = $element->name;
			endforeach;
			return $elements;
		}
	}
