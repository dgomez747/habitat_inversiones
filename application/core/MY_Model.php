<?php

class MY_Model extends CI_Model {
	public function insert($data) {
		try {
			$this->db->trans_begin();
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			$this->db->trans_commit();
			return $id;
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return null;
		}
	}

	public function insert_lote($data) {
		$this->db->insert_batch($this->table, $data);
	}

	public function update($pk, $data) {
		$this->db->where($this->table_id, $pk);
		$this->db->update($this->table, $data);
		if ($this->db->affected_rows() == '1') {
			return 1;
		} else {
			// any trans error?
			if ($this->db->trans_status() === false) {
				return 0;
			}
			return 1;
		}
	}

	public function delete($pk) {
		$this->db->where($this->table_id, $pk);
		$this->db->delete($this->table);
	}

	public function get_search_row($data) {
		if (isset($data["select"]) && $data["select"] != "") {
			$this->db->select($data["select"]);
		}
		if (isset($data["where"]) && $data["where"] != "") {
			$this->db->where($data["where"]);
		}
		if (isset($data["order"]) && $data["order"] != "") {
			$this->db->order_by($data["order"]);
		}
		if (isset($data["group"]) && $data["group"] != "") {
			$this->db->group_by($data["group"]);
		}
		if (isset($data["join"])) {
			if (count($data["join"]) > 0) {
				foreach ($data["join"] as $rowJoin) {
					$split = explode(",", $rowJoin);
					$this->db->join($split[0], $split[1], isset($split[2]) ? $split[2] : "");
				}
			}
		}
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->row();
	}

	public function search($data) {
		if (isset($data["select"]) && $data["select"] != "") {
			$this->db->select($data["select"]);
		}
		if (isset($data["where"]) && $data["where"] != "") {
			$this->db->where($data["where"]);
		}
		if (isset($data["order"]) && $data["order"] != "") {
			$this->db->order_by($data["order"]);
		}
		if (isset($data["group"]) && $data["group"] != "") {
			$this->db->group_by($data["group"]);
		}
		if (isset($data["having"]) && $data["having"] != "") {
			$this->db->having($data["having"]);
		}
		if (isset($data["join"])) {
			if (count($data["join"]) > 0) {
				foreach ($data["join"] as $rowJoin) {
					$split = explode(",", $rowJoin);
					$this->db->join($split[0], $split[1], isset($split[2]) ? $split[2] : "");
				}
			}
		}
		$this->db->from($this->table);
		$query = $this->db->get();
		$dato = $query->result();
		return $dato;
	}

	public function total_records($data) {
		if (isset($data["select"]) && $data["select"] != "") {
			$this->db->select($data["select"]);
		}
		if (isset($data["where"]) && $data["where"] != "") {
			$this->db->where($data["where"]);
		}
		if (isset($data["order"]) && $data["order"] != "") {
			$this->db->order_by($data["order"]);
		}
		if (isset($data["group"]) && $data["group"] != "") {
			$this->db->group_by($data["group"]);
		}
		if (isset($data["join"])) {
			if (count($data["join"]) > 0) {
				foreach ($data["join"] as $rowJoin) {
					$split = explode(",", $rowJoin);
					$this->db->join($split[0], $split[1], isset($split[2]) ? $split[2] : "");
				}
			}
		}
		$this->db->from($this->table);
		$query = $this->db->get();
		$dato = $query->num_rows();
		return $dato;
	}

	public function search_data($data, $inicio, $num_reg) {
		if (isset($data["select"]) && $data["select"] != "") {
			$this->db->select($data["select"]);
		}
		if (isset($data["where"]) && $data["where"] != "") {
			$this->db->where($data["where"]);
		}
		if (isset($data["order"]) && $data["order"] != "") {
			$this->db->order_by($data["order"]);
		}
		if (isset($data["group"]) && $data["group"] != "") {
			$this->db->group_by($data["group"]);
		}
		if (isset($data["join"])) {
			if (count($data["join"]) > 0) {
				foreach ($data["join"] as $rowJoin) {
					$split = explode(",", $rowJoin);
					$this->db->join($split[0], $split[1], isset($split[2]) ? $split[2] : "");
				}
			}
		}
		$this->db->from($this->table);
		$query = $this->db->get("", $inicio, $num_reg);
		$dato = $query->result();
		return $dato;
	}

	public function search_data_rows($data, $inicio, $num_reg) {
		if (isset($data["select"]) && $data["select"] != "") {
			$this->db->select($data["select"]);
		}
		if (isset($data["where"]) && $data["where"] != "") {
			$this->db->where($data["where"]);
		}
		if (isset($data["order"]) && $data["order"] != "") {
			$this->db->order_by($data["order"]);
		}
		if (isset($data["group"]) && $data["group"] != "") {
			$this->db->group_by($data["group"]);
		}
		if (isset($data["join"])) {
			if (count($data["join"]) > 0) {
				foreach ($data["join"] as $rowJoin) {
					$split = explode(",", $rowJoin);
					$this->db->join($split[0], $split[1], isset($split[2]) ? $split[2] : "");
				}
			}
		}
		$this->db->from($this->table);
		$query = $this->db->get("", $inicio, $num_reg);
		$dato = $query->row();
		return $dato;
	}
}
