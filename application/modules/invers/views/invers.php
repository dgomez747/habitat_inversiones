<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
<?php
	foreach ($blocks as $block):
		if ($block->status == 1):

			$back_color = "";

			if ($block->background_color):
				$back_color = 'background-color:' . $block->background_color . '; ';
			endif;

			$style_bk= 'style="' . $back_color . '"'
		?>

		<section <?= $style_bk ?> >
			<h1><?=  "Bloque: {$block->name}"?></h1>
			<?php foreach ($block->values as $value):
				$text_color = "";
				$background_color = "";

				if ($value->text_color):
					$text_color = 'color:' . $value->text_color . '; ';
				endif;

				if ($value->background_color):
					$background_color = 'background-color:' . $value->background_color . '; ';
				endif;

				$style = 'style="' . $text_color . $background_color . '"';

				$grafico = null;
				if($value->type_id == 4):
					if(isset($value->graphic)):
						$grafico = $value->graphic;
					endif;
				endif;

			?>
				<p <?= $style ?> ><?= "<span>{$value->name}: {$value->value}"?></p>
				<?php if($grafico):
						echo "<pre>";
						print_r($grafico);
				endif ?>
			<?php endforeach; ?>
		</section>
			<hr>
		<?php endif;
	endforeach;
?>