<?php if (!defined("BASEPATH"))
	exit("No direct script access allowed");

	class Scheme_model extends MY_Model {
		public function __construct() {
			parent::__construct();
			$this->column = ['scheme.id', 'type.name', 'scheme.value', 'scheme.position', 'scheme.status', 'scheme.text_color', 'scheme.background_color', 'scheme.graphic_table_name', 'scheme.type_id'];
			$this->column_search = ['type.name', 'scheme.value', 'scheme.position'];
			$this->table = 'scheme';
			$this->table_id = 'id';
		}

		public function get($where = null, $first = false) {
			if ($where):
				$this->db->where($where);
			endif;
			$this->db->from($this->table);
			$query = $this->db->get();
			return $first ? $query->row() : $query->result();
		}

		public function _get_datatables_query($where = null) {
			$this->db->select($this->column);
			$this->db->from($this->table);
			$this->db->join('block', "block.id = " . $this->table . ".block_id");
			$this->db->join('type', "type.id = " . $this->table . ".type_id");
			if ($where):
				$this->db->where($where);
			endif;
			foreach ($this->column_search as $key => $item):
				if ($_GET['search']['value']):
					if ($key === 0):
						$this->db->group_start();
						$this->db->like($item, $_GET['search']['value']);
					else:
						$this->db->or_like($item, $_GET['search']['value']);
					endif;
					if (count($this->column_search) - 1 == $key):
						$this->db->group_end();
					endif;
				endif;
			endforeach;
			if (isset($_GET['order'])):
				$this->db->order_by($this->column_order[$_GET['order']['0']['column']], $_GET['order']['0']['dir']);
			elseif (isset($this->order)):
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			endif;
		}

		function count_filtered() {
			$this->_get_datatables_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all() {
			$this->db->from($this->table);
			return $this->db->count_all_results();
		}

		public function joiner($where = null, $first = false, $order = null) {
			$this->db->select($this->column);
			$this->db->from($this->table);
			$this->db->join('type', "type.id = " . $this->table . ".type_id");
			if ($where):
				$this->db->where($where);
			endif;
			if ($order):
				foreach ($order as $index => $value):
					$this->db->order_by($index, $value);
				endforeach;
			endif;
			$query = $this->db->get();
			return $first ? $query->row() : $query->result();
		}

		function get_datatables($where = null) {
			$this->_get_datatables_query($where);
			if ($_GET['length'] != -1):
				$this->db->limit($_GET['length'], $_GET['start']);
			endif;
			$this->db->order_by('position', 'asc');
			$query = $this->db->get();
			return $query->result();
		}

		public function destroy($where) {
			$this->db->where($where);
			return $this->db->delete($this->table);
		}
	}
