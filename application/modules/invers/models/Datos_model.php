<?php if (!defined("BASEPATH"))
	exit("No direct script access allowed");

	class Datos_model extends MY_Model {
		public function __construct() {
			parent::__construct();
			// $this->table = 'fondo_afp_periodo';
			// $this->table_id = 'id';
		}

		public function get($table, $where = null, $first = false, $order = null) {
			if ($where):
				$this->db->where($where);
			endif;
			$this->db->from($table);
			if($order):
				foreach ($order as $index => $value):
					$this->db->order_by($index, $value);
				endforeach;
			endif;
			$query = $this->db->get();
			return $first ? $query->row() : $query->result();
		}

		public function getDatosMeses()
    {
			// $this->db->where('periodo_id', 1)->where('fondo_id', 1);
			$this->db->order_by("mes_value", "DESC");
			$this->db->order_by("order", "asc");

			$q = $this->db->get('grafico_2');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data[$row->mes_value][] = $row; //['mes' => $row->mes, 'value' => $row->value, 'color' => $row->color];
				}

				return $data;
			}
			return false;
		}

		public function getDatosInvert()
    {
			$q = $this->db->get('grafico_5');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function getAFPs()
    {

			$q = $this->db->get('afp');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data[] = ['id' => $row->id, 'name' => $row->name, 'prom' => $row->prom, 'status' => $row->status];
				}

				return $data;
			}
			return false;
		}

		public function getInversiones()
    {

			$this->db->where_in('id', [1,2]);
			$q = $this->db->get('inversion');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function getCartera()
    {

			$this->db->where_not_in('id', [1,2]);
			$q = $this->db->get('inversion');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function getDatosCartera()
    {
			$q = $this->db->get('grafico_6');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {
					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function getAFPsColor()
    {

			$q = $this->db->get('afp');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data[] = $row; //['id' => $row->id, 'name' => $row->name, 'prom' => $row->prom, 'status' => $row->status];
				}

				return $data;
			}
			return false;
		}

		public function getPuestos()
    {

			$q = $this->db->get('puesto');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {
					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function getTipoRenta()
    {

			$q = $this->db->get('tipo_renta');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {
					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function getDatosPercRenta()
    {

			$q = $this->db->get('grafico_4');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {
					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function getFondos()
    {

			$color = $this->getColorConfig('fondo');

			$q = $this->db->get('fondo');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data['colors'] = $color;
					$data['listOfFunds'][] = $row;

				}

				return $data;
			}
			return false;
		}

		public function getPeriodos()
    {

			$color = $this->getColorConfig('periodo');

			$q = $this->db->get('periodo');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {

					$data['colors'] = $color;
					$data['listOfPeriods'][] = $row;

				}

				return $data;
			}
			return false;
		}

		public function getColorConfig($table)
    {
			$q = $this->db->get_where('colorConfig', array('table' => $table), 1);
			if ($q->num_rows() > 0) {
					return $q->row();
			}
			return false;
		}

		public function getDatos12Meses()
    {
			$q = $this->db->get('grafico_3');
			if ($q->num_rows() > 0) {

				foreach (($q->result()) as $key => $row) {
					$data[] = $row;
				}

				return $data;
			}
			return false;
		}

		public function test()
    {
			$q = $this->db->get('test');
			if ($q->num_rows() > 0) {
				foreach (($q->result()) as $key => $row) {
					$data = $row->json;
				}
				return $data;
			}
			return false;
		}



	}
