<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'core/MY_InversController.php';

class Invers extends MY_InversController
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['invers/Block_model' => 'Block', 'invers/Scheme_model' => 'Scheme']);
		$this->load->model('invers/Datos_model', 'obj_datos');
	}

	public function index()
	{
		$blocks = $this->Block->get(['block.status' => 1], false, ['position' => 'asc']);
		foreach ($blocks as $block) :
			$block->values = $this->Scheme->joiner(['scheme.block_id' => $block->id, 'scheme.status' => 1], null, ['scheme.position' => 'asc']);
			foreach ($block->values as $value) :
				if ($value->type_id == 4) :
					if ($value->graphic_table_name == "grafico_1") :
						$value->graphic = $this->grafico_1();
						// $value->graphic = $this->grafico($value->graphic_table_name);
					elseif($value->graphic_table_name == "grafico_2"):
						$value->graphic = $this->grafico_2();
					elseif($value->graphic_table_name == "grafico_3"):
						$value->graphic = $this->grafico_3();
					elseif($value->graphic_table_name == "grafico_4"):
					$value->graphic = $this->grafico_4();
					elseif($value->graphic_table_name == "grafico_5"):
					$value->graphic = $this->grafico_5();
					elseif($value->graphic_table_name == "grafico_6"):
						$value->graphic = $this->grafico_6();
					endif;

					$graficos[$value->graphic_table_name] = $value->graphic;

				endif;
			endforeach;
		endforeach;

		// $this->load->view('invers', ['blocks' => $blocks]);
		// $this->response_send(['status' => 200, 'response' => $graficos]);
		$this->response_send(['status' => 200, 'response' => $blocks]);
		// $this->output->enable_profiler(true);
	}

	// public function grafico($graphic_table_name)
	// {
	// 	$data = $this->obj_datos->get($graphic_table_name);
	// 	return $data;
	// }

	public function test(){
		$data = $this->obj_datos->test();
		echo $data;
	}

	public function grafico_1()
	{

		$data =
		[
			"afps" => [
				[
					"id" => "1",
					"name" => "Habitat",
					"color" => "#D90034"
				],
				[
					"id" => "2",
					"name" => "Integra",
					"color" => "#87968E"
				],
				[
					"id" => "3",
					"name" => "Prima",
					"color" => "#87968E"
				],
				[
					"id" => "4",
					"name" => "Profuturo",
					"color" => "#87968E"
				],
				[
					"id" => "5",
					"name" => "PROMEDIO DEL SPP",
					"color" => "#D90034"
				]
			],
			"funds" => [
				"colors" => [
					"default" => "#dsdds",
					"active" => "#dsdds",
					"defautText" => "#dsdds",
					"activeText" => "#dsdds"
				],
				"list" => [
					[
						"id" => "1",
						"name"  =>"Fondo 1"
					],
					[
						"id" => "2",
						"name"  =>"Fondo 2"
					],
					[
						"id" => "3",
						"name"  =>"Fondo 3"
					]
				]
			],
			"periods" => [
				"colors" => [
					"default" => "#dsdds",
					"active" => "#dsdds",
					"defautText" => "#dsdds",
					"activeText" => "#dsdds"
				],
				"list" => [
					[
						"id" => "1",
						"name" => "1 años"
					],
					[
						"id" => "2",
						"name" => "2 años"
					],
					[
						"id" => "3",
						"name" => "3 años"
					],
					[
						"id" => "4",
						"name" => "4 años"
					],
					[
						"id" => "5",
						"name" => "5 años"
					]
				]
			],
			"chart" => [
				"bar" => [
					[
						"afpId" => "1",
						"fundId" => "1",
						"periodId" => "1",
						"value" => "21"
					]
				]
			]
		];

		return $data;
	}

	public function grafico_2()
	{

		$meses = $this->obj_datos->getDatosMeses();
		$afps = $this->obj_datos->getAFPs();
		$puestos = $this->obj_datos->getPuestos();
		$fondos = $this->obj_datos->getFondos();
		$periodos = $this->obj_datos->getPeriodos();

		$data = array(
			'meses' => $meses,
			'afps' => $afps,
			'puestos' => $puestos,
			'fondos' => $fondos,
			'periodos' => $periodos,
		);

		return $data;
	}

	public function grafico_3()
	{

		$datos = $this->obj_datos->getDatos12Meses();
		$afps = $this->obj_datos->getAFPsColor();

		$data = array(
			'datos' => $datos,
			'afps' => $afps,
		);

		return $data;
	}

	public function grafico_4()
	{

		$datos = $this->obj_datos->getDatosPercRenta();
		$renta = $this->obj_datos->getTipoRenta();
		$fondos = $this->obj_datos->getFondos();

		$data = array(
			'datos' => $datos,
			'renta' => $renta,
			'fondos' => $fondos,
		);

		return $data;
	}

	public function grafico_5()
	{

		$datos = $this->obj_datos->getDatosInvert();
		$inversion = $this->obj_datos->getInversiones();
		$fondos = $this->obj_datos->getFondos();

		$data = array(
			'datos' => $datos,
			'renta' => $inversion,
			'fondos' => $fondos,
		);

		return $data;
	}

	public function grafico_6()
	{

		$datos = $this->obj_datos->getDatosCartera();
		$inversion = $this->obj_datos->getCartera();
		$fondos = $this->obj_datos->getFondos();

		$data = array(
			'datos' => $datos,
			'renta' => $inversion,
			'fondos' => $fondos,
		);

		return $data;
	}

}
