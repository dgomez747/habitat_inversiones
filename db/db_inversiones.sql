-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-03-2019 a las 20:41:30
-- Versión del servidor: 5.6.15-log
-- Versión de PHP: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_inversiones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afp`
--

CREATE TABLE IF NOT EXISTS `afp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `color` varchar(45) DEFAULT NULL,
  `prom` int(11) DEFAULT '0' COMMENT '0: afp \\n1: promedio',
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `afp`
--

INSERT INTO `afp` (`id`, `name`, `color`, `prom`, `status`) VALUES
(1, 'HABITAT', '#CE2727', 0, 1),
(2, 'INTEGRA', '#579BBE', 0, 1),
(3, 'PRO- FUTURO', '#394D60', 0, 1),
(4, 'PRIMA', '#1868B7', 0, 1),
(5, 'PROMEDIO  - SPP', '#CE2727', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `block`
--

CREATE TABLE IF NOT EXISTS `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `background_color` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `view` int(1) DEFAULT '0' COMMENT '0:Mobile y Desktop \\n1:Solo Mobile \\n2:Solo Desktop',
  `status` int(1) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `block`
--

INSERT INTO `block` (`id`, `name`, `reference`, `background_color`, `position`, `view`, `status`) VALUES
(1, 'Te presentamos la rentabilidad de nuestros fondos', 'rentabilidad_fondo_afp', NULL, 1, 0, 1),
(2, 'Banner 1', 'banner', '#1868B7', 2, 0, 1),
(3, 'Ranking de Rentabilidad Anualizada', 'rentabilidad_anualizada', NULL, 3, 0, 1),
(4, 'Fondos en soles captados por AFP', 'fondos_afp', NULL, 4, 0, 1),
(5, 'Banner 2', 'banner', '#1868B7', 5, 0, 1),
(6, 'Distribución de fondo por tipo de Renta', 'fondo_renta', NULL, 6, 0, 1),
(7, '¿En qué invirtió AFP Habitat?', 'invirtio', NULL, 7, 0, 1),
(8, 'Banner 3', 'banner', '#E4F2ED', 8, 0, 1),
(9, 'Cartera Administrada', 'cartera_admin', NULL, 9, 0, 1),
(10, 'Banner 4', 'banner', '#E4F2ED', 10, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colorconfig`
--

CREATE TABLE IF NOT EXISTS `colorconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(45) DEFAULT NULL,
  `default` varchar(45) DEFAULT NULL,
  `active` varchar(45) DEFAULT NULL,
  `defautText` varchar(45) DEFAULT NULL,
  `activeText` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `colorconfig`
--

INSERT INTO `colorconfig` (`id`, `table`, `default`, `active`, `defautText`, `activeText`, `status`) VALUES
(1, 'fondo', '#6C6B6C', '#D90034', '#6C6B6C', '#D90034', 1),
(2, 'periodo', '#394D60', '#D90034', '#FFFFFF', '#FFFFFF', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fondo`
--

CREATE TABLE IF NOT EXISTS `fondo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `fondo`
--

INSERT INTO `fondo` (`id`, `name`, `status`) VALUES
(1, 'Fondo 1', 1),
(2, 'Fondo 2', 1),
(3, 'Fondo 3', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grafico_2`
--

CREATE TABLE IF NOT EXISTS `grafico_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periodo_id` int(11) NOT NULL,
  `fondo_id` int(11) NOT NULL,
  `afp_id` int(11) NOT NULL,
  `puesto_id` int(11) DEFAULT NULL,
  `mes_value` varchar(45) NOT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`),
  KEY `fk_grafico_1_periodo1_idx` (`periodo_id`),
  KEY `fk_grafico_2_fondo1_idx` (`fondo_id`),
  KEY `fk_grafico_2_afp1_idx` (`afp_id`),
  KEY `fk_grafico_2_puesto1_idx` (`puesto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `grafico_2`
--

INSERT INTO `grafico_2` (`id`, `periodo_id`, `fondo_id`, `afp_id`, `puesto_id`, `mes_value`, `mes`, `value`, `color`, `order`, `status`) VALUES
(1, 1, 1, 1, 1, '201808', 'Ago 18', '5.66', '#5AB781', 1, 1),
(2, 1, 1, 1, 1, '201807', 'Jul 18', '5.66', '#5AB781', 1, 1),
(3, 1, 1, 1, 1, '201806', 'Jun 18', '5.66', '#5AB781', 1, 1),
(4, 1, 1, 2, 2, '201808', 'Ago 18', '5.66', '#DBDB45', 2, 1),
(5, 1, 1, 2, 2, '201807', 'Jul 18', '5.66', '#DBDB45', 2, 1),
(6, 1, 1, 2, 2, '201806', 'Jun 18', '5.66', '#DBDB45', 2, 1),
(7, 1, 1, 3, 3, '201808', 'Ago 18', '5.66', '#F1692A', 3, 1),
(8, 1, 1, 3, 3, '201807', 'Jul 18', '5.66', '#F1692A', 3, 1),
(9, 1, 1, 3, 2, '201806', 'Jun 18', '5.66', '#F1692A', 3, 1),
(10, 1, 1, 4, 2, '201808', 'Ago 18', '5.66', '#CE2727', 4, 1),
(11, 1, 1, 4, 2, '201807', 'Jul 18', '5.66', '#CE2727', 4, 1),
(12, 1, 1, 4, 3, '201806', 'Jun 18', '5.66', '#CE2727', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grafico_3`
--

CREATE TABLE IF NOT EXISTS `grafico_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `afp_id` int(11) NOT NULL,
  `value` varchar(45) NOT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`),
  KEY `fk_grafico_3_afp1_idx` (`afp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `grafico_3`
--

INSERT INTO `grafico_3` (`id`, `afp_id`, `value`, `status`) VALUES
(1, 1, '21', 1),
(2, 2, '-8.2', 1),
(3, 4, '9.2', 1),
(4, 3, '-9', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grafico_4`
--

CREATE TABLE IF NOT EXISTS `grafico_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fondo_id` int(11) NOT NULL,
  `tipo_renta_id` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`),
  KEY `fk_grafico_4_fondo1_idx` (`fondo_id`),
  KEY `fk_grafico_4_tipo_renta1_idx` (`tipo_renta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `grafico_4`
--

INSERT INTO `grafico_4` (`id`, `fondo_id`, `tipo_renta_id`, `value`, `status`) VALUES
(1, 1, 1, '90.4', 1),
(2, 1, 2, '9.58', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grafico_5`
--

CREATE TABLE IF NOT EXISTS `grafico_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fondo_id` int(11) NOT NULL,
  `inversion_id` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`),
  KEY `fk_grafico_5_fondo1_idx` (`fondo_id`),
  KEY `fk_grafico_5_inversiones1_idx` (`inversion_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `grafico_5`
--

INSERT INTO `grafico_5` (`id`, `fondo_id`, `inversion_id`, `value`, `status`) VALUES
(1, 1, 3, '22.9', 1),
(2, 1, 4, '26.1', 1),
(3, 1, 5, '26.1', 1),
(4, 1, 6, '74.9', 1),
(5, 1, 7, '22.9', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grafico_6`
--

CREATE TABLE IF NOT EXISTS `grafico_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fondo_id` int(11) NOT NULL,
  `inversion_id` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`),
  KEY `fk_grafico_5_fondo1_idx` (`fondo_id`),
  KEY `fk_grafico_5_inversiones1_idx` (`inversion_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `grafico_6`
--

INSERT INTO `grafico_6` (`id`, `fondo_id`, `inversion_id`, `value`, `status`) VALUES
(1, 1, 1, '74.9', 1),
(2, 1, 2, '26.1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inversion`
--

CREATE TABLE IF NOT EXISTS `inversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `inversion`
--

INSERT INTO `inversion` (`id`, `name`, `color`, `status`) VALUES
(1, 'Inversiones locales', '#D90034', 1),
(2, 'Invesiones en el exterior', '#1868B7', 1),
(3, 'Gobierno', '#579BBE', 1),
(4, 'Sistema financiero', '#1868B7', 1),
(5, 'Sociedades titulizadoras', '#57645F', 1),
(6, 'Empresas no financieras', '#D90034', 1),
(7, 'Administradoras de fondos', '#394D60', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE IF NOT EXISTS `modulo` (
  `id_modulo` int(10) NOT NULL AUTO_INCREMENT,
  `id_modulo_parent` int(10) DEFAULT NULL,
  `name` varchar(350) DEFAULT NULL,
  `url` varchar(350) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `weight` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_modulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodo`
--

CREATE TABLE IF NOT EXISTS `periodo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `periodo`
--

INSERT INTO `periodo` (`id`, `name`, `status`) VALUES
(1, '5 años', 1),
(2, '4 años', 1),
(3, '3 años', 1),
(4, '2 años', 1),
(5, '1 años', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `id_permission` int(10) NOT NULL AUTO_INCREMENT,
  `id_rol` int(11) DEFAULT NULL,
  `id_modulo` int(10) DEFAULT NULL,
  `permission` varchar(45) DEFAULT NULL COMMENT '1 : ver\\\\\\\\n2 : crear\\\\\\\\n3 : editar\\\\\\\\n4 : eliminar',
  PRIMARY KEY (`id_permission`),
  KEY `fk_rol_idx` (`id_rol`),
  KEY `fk_modulo_idx` (`id_modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puesto`
--

CREATE TABLE IF NOT EXISTS `puesto` (
  `id` int(11) NOT NULL,
  `orden_puesto` int(11) DEFAULT NULL,
  `puesto` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puesto`
--

INSERT INTO `puesto` (`id`, `orden_puesto`, `puesto`, `color`) VALUES
(1, 1, '1er', '#5AB781'),
(2, 2, '2do', '#DBDB45'),
(3, 3, '3er', '#F1692A'),
(4, 4, '4to', '#CE2727'),
(5, 5, '5to', '#394D60');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `rol_name` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 : activo\\n0 : inactivo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`, `rol_name`, `status`) VALUES
(1, 'admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scheme`
--

CREATE TABLE IF NOT EXISTS `scheme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `graphic_table_name` varchar(45) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `download` varchar(200) DEFAULT NULL,
  `icon` varchar(20) DEFAULT NULL,
  `text_color` varchar(50) DEFAULT NULL,
  `background_color` varchar(50) DEFAULT NULL,
  `active_color` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `view` int(1) DEFAULT '0' COMMENT '0:Mobile y Desktop \\n1:Solo Mobile \\n2:Solo Desktop',
  `status` int(1) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`),
  KEY `fk_scheme_type1_idx` (`type_id`),
  KEY `fk_scheme_block1_idx` (`block_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Volcado de datos para la tabla `scheme`
--

INSERT INTO `scheme` (`id`, `block_id`, `type_id`, `value`, `graphic_table_name`, `link`, `download`, `icon`, `text_color`, `background_color`, `active_color`, `position`, `view`, `status`) VALUES
(1, 1, 1, 'Conoce los resultados de ', NULL, NULL, NULL, NULL, '#909092', NULL, NULL, 1, 0, 1),
(3, 1, 1, 'por tipo de fondo y por AFP', NULL, NULL, NULL, NULL, '#909092', NULL, NULL, 3, 0, 1),
(4, 1, 4, 'Gráfico 1', 'grafico_1', NULL, NULL, NULL, NULL, NULL, NULL, 7, 0, 1),
(6, 1, 2, 'rentabilidad', NULL, 'https://', NULL, NULL, '#1b6ab7', NULL, NULL, 2, 0, 1),
(8, 2, 1, 'Cámbiate a la AFP N°1 en Rentabilidad', NULL, NULL, NULL, NULL, '#FFFFFF', NULL, NULL, 0, 0, 1),
(9, 2, 3, 'Cambiate aquí', NULL, 'https://', NULL, NULL, '#FFFFFF', '#D90034', NULL, 2, 0, 1),
(31, 1, 3, 'Fuente legal de la información', NULL, 'https://', NULL, NULL, '#FFFFFF', '#4A4A4A', NULL, 13, 0, 1),
(35, 5, 1, 'Cámbiate a la AFP N°1 en Rentabilidad', NULL, NULL, NULL, NULL, '#FFFFFF', NULL, NULL, 0, 0, 1),
(36, 5, 3, 'Cambiate aquí', NULL, 'https://', NULL, NULL, '#FFFFFF', '#D90034', NULL, 2, 0, 1),
(56, 3, 4, 'Gráfico 2', 'grafico_2', NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, 1),
(57, 4, 4, 'Gráfico 3', 'grafico_3', NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, 1),
(58, 6, 4, 'Gráfico 4', 'grafico_4', NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, 1),
(59, 7, 4, 'Gráfico 5', 'grafico_5', NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, 1),
(60, 9, 4, 'Gráfico 6', 'grafico_6', NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `json` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `test`
--

INSERT INTO `test` (`id`, `json`) VALUES
(1, '{\r\n  "afps": [\r\n    {\r\n      "id": "1",\r\n      "name": "Habitat",\r\n      "color": "#D90034"\r\n    },\r\n    {\r\n      "id": "2",\r\n      "name": "Integra",\r\n      "color": "#87968E"\r\n    },\r\n    {\r\n      "id": "3",\r\n      "name": "Prima",\r\n      "color": "#87968E"\r\n    },\r\n    {\r\n      "id": "4",\r\n      "name": "Profuturo",\r\n      "color": "#87968E"\r\n    },\r\n    {\r\n      "id": "5",\r\n      "name": "PROMEDIO DEL SPP",\r\n      "color": "#D90034"\r\n    }\r\n  ],\r\n  "funds": {\r\n    "colors": {\r\n      "default": "#dsdds",\r\n      "active": "#dsdds",\r\n      "defautText": "#dsdds",\r\n      "activeText": "#dsdds"\r\n    },\r\n    "list": [\r\n      {\r\n        "id": "1",\r\n        "name" :"Fondo 1"\r\n      },\r\n      {\r\n        "id": "2",\r\n        "name" :"Fondo 2"\r\n      },\r\n      {\r\n        "id": "3",\r\n        "name" :"Fondo 3"\r\n      }\r\n    ]\r\n  },\r\n  "periods": {\r\n    "colors": {\r\n      "default": "#dsdds",\r\n      "active": "#dsdds",\r\n      "defautText": "#dsdds",\r\n      "activeText": "#dsdds"\r\n    },\r\n    "list": [\r\n      {\r\n        "id": "1",\r\n        "name": "1 años"\r\n      },\r\n      {\r\n        "id": "2",\r\n        "name": "2 años"\r\n      },\r\n      {\r\n        "id": "3",\r\n        "name": "3 años"\r\n      },\r\n      {\r\n        "id": "4",\r\n        "name": "4 años"\r\n      },\r\n      {\r\n        "id": "5",\r\n        "name": "5 años"\r\n      }\r\n    ]\r\n  },\r\n  "chart": {\r\n    "bar": [\r\n      {\r\n        "afpId": "1",\r\n        "fundId": "1",\r\n        "periodId": "1",\r\n        "value": "21"\r\n      }\r\n    ]\r\n  }\r\n}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_renta`
--

CREATE TABLE IF NOT EXISTS `tipo_renta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `color` varchar(45) DEFAULT NULL,
  `coment_title` varchar(45) DEFAULT NULL,
  `coment_text` varchar(200) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_renta`
--

INSERT INTO `tipo_renta` (`id`, `name`, `color`, `coment_title`, `coment_text`, `status`) VALUES
(1, 'Renta Fija', '#D90034', '¿Qué es la renta fija?', 'Instrumentos financieros de bajo riesgo invertidos por un periodo de tiempo determinado. Están asociados a bonos de empresas.', 1),
(2, 'Renta Variable', '#1868B7', '¿Qué es la renta variable?', 'Están asociados a los movimientos financieros que se realizan en los mercados más volátiles, por ejemplo: Bolsa de valores. Es por ello que este tipo de inversión presenta mayores riesgos.', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 : activo \\n0 : inactivo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `type`
--

INSERT INTO `type` (`id`, `name`, `status`) VALUES
(1, 'Texto', 1),
(2, 'Link', 1),
(3, 'Botón', 1),
(4, 'Gráfico', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 : activo\\\\n0 : inactivo',
  PRIMARY KEY (`username`),
  KEY `fk_user_role_idx` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`username`, `role_id`, `password`, `name`, `surname`, `deleted_at`, `status`) VALUES
('admin', 1, '4ac2184ba55935cc411290de766b492e', 'Admin', 'Admin', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_modulo`
--

CREATE TABLE IF NOT EXISTS `user_modulo` (
  `id_user_modulo` int(10) NOT NULL AUTO_INCREMENT,
  `id_modulo` int(10) DEFAULT NULL,
  `id_user` varchar(50) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `permission` varchar(45) DEFAULT NULL COMMENT '1 : ver\\\\\\\\n2 : crear\\\\\\\\n3 : editar\\\\\\\\n4 : eliminar',
  PRIMARY KEY (`id_user_modulo`),
  KEY `fk_modulo_idx` (`id_modulo`),
  KEY `fk_user_idx` (`id_user`),
  KEY `fk_rol_idx` (`id_rol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `grafico_2`
--
ALTER TABLE `grafico_2`
  ADD CONSTRAINT `fk_grafico_1_periodo1` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grafico_2_afp1` FOREIGN KEY (`afp_id`) REFERENCES `afp` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grafico_2_fondo1` FOREIGN KEY (`fondo_id`) REFERENCES `fondo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grafico_2_puesto1` FOREIGN KEY (`puesto_id`) REFERENCES `puesto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grafico_3`
--
ALTER TABLE `grafico_3`
  ADD CONSTRAINT `fk_grafico_3_afp1` FOREIGN KEY (`afp_id`) REFERENCES `afp` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grafico_4`
--
ALTER TABLE `grafico_4`
  ADD CONSTRAINT `fk_grafico_4_fondo1` FOREIGN KEY (`fondo_id`) REFERENCES `fondo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grafico_4_tipo_renta1` FOREIGN KEY (`tipo_renta_id`) REFERENCES `tipo_renta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grafico_5`
--
ALTER TABLE `grafico_5`
  ADD CONSTRAINT `fk_grafico_5_fondo1` FOREIGN KEY (`fondo_id`) REFERENCES `fondo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grafico_5_inversiones1` FOREIGN KEY (`inversion_id`) REFERENCES `inversion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grafico_6`
--
ALTER TABLE `grafico_6`
  ADD CONSTRAINT `fk_grafico_5_fondo10` FOREIGN KEY (`fondo_id`) REFERENCES `fondo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grafico_5_inversiones10` FOREIGN KEY (`inversion_id`) REFERENCES `inversion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permission`
--
ALTER TABLE `permission`
  ADD CONSTRAINT `fk_permission_modulo1` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id_modulo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_permission_role1` FOREIGN KEY (`id_rol`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `scheme`
--
ALTER TABLE `scheme`
  ADD CONSTRAINT `fk_scheme_block1` FOREIGN KEY (`block_id`) REFERENCES `block` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_scheme_type1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_modulo`
--
ALTER TABLE `user_modulo`
  ADD CONSTRAINT `fk_modulo_3` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id_modulo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rol_3` FOREIGN KEY (`id_rol`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_modulo_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
